import * as convert from "color-convert";

class ColorFunctions {
    private static redYellowHue = [350, 64];


    public static getRedYellowLoop(fraction : number) {
        if(fraction < 0) {
            fraction = 0;
        } else if(fraction > 1) {
            fraction = 1;
        }
        
        let [lower, upper] = this.redYellowHue
        if(lower > upper) {
            upper += 360;
        }
        
        const hue = (lower + (upper - lower) * fraction) % 360;
        console.info(hue)
        return "#"+convert.hsv.hex([Math.floor(hue), 100, 100])
    }
}

export default ColorFunctions;