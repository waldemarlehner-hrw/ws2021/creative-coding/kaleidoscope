import p5 from "p5";
import ColorFunctions from "./ColorFunctions";

class KaleidoscopeSketch {
	
	private canvas?: p5.Renderer;

	private screenSize = 800;
	private drawerCount = 20;
	private colorFunction = (fraction: number) => ColorFunctions.getRedYellowLoop(fraction);
	private pencilSize = 20;
	
	private counter = 0;
	private colorSpeedMultiplier = 0.02
	private center = {x: this.screenSize / 2, y: this.screenSize / 2 }
	private isMousePressed: boolean = false
	private colorInfo?: HTMLElement | undefined;

	constructor(private p: p5){
		this.colorInfo = document.querySelector("div#colorInfo") as HTMLElement ?? undefined;
		p.setup = () => this.setup();
		p.draw = () => this.draw();
		p.mousePressed = () => this.mousePressed();
		p.mouseReleased = () => this.mouseReleased();
	}

	private mousePressed() {
		this.isMousePressed = true;
	}

	private mouseReleased() {
		this.isMousePressed = false;
	}

	private setup() {
		this.canvas = this.p.createCanvas(this.screenSize, this.screenSize);
		this.canvas.parent("app");
		this.p.background("#FFFFFF");
	}

	private draw() {
		this.counter++;
		const color = this.colorFunction((Math.sin(this.counter * this.colorSpeedMultiplier) + 1) / 2);
		if(this.colorInfo?.style) {
			this.colorInfo.style.backgroundColor = color;
		}
		if(!this.isMousePressed){
			return;
		}

		const rotationPerBrush = 2 * Math.PI / this.drawerCount;
		
		const dX = - this.center.x + this.p.mouseX;
		const dY = - this.center.y + this.p.mouseY;

		
		for( let i = 0; i < this.drawerCount; i++) {
			this.p.push()
			this.p.fill(color)
			this.p.translate(this.center.x, this.center.y);
			this.p.rotate(rotationPerBrush * i)
			this.p.circle(dX, dY, this.pencilSize)
			this.p.pop()
		}
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new KaleidoscopeSketch(p5);
	}
}

export default KaleidoscopeSketch;
