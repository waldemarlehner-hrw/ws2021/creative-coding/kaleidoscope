import './style.css'
import P5 from "p5";
import KaleidoscopeSketch from "./KaleidoscopeSketch"

const sketch = KaleidoscopeSketch.toP5Sketch();

new P5(sketch);
